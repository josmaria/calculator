function Button(props) {
  
  const isOperator = value => {
    return isNaN(value) && (value != '.') && (value != '=');
  };
  
  return (
    <div className={`button-conteiner ${isOperator(props.children) ? 'operator' : ''}`.trimEnd()}>
      {props.children}
    </div>
  );
}

export default Button;