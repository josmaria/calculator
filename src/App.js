import './App.css';
import calculatorLogo from './images/calculator-logo.png'
import Button from './components/Button.jsx'

function App() {
  return (
    <div className="App">
      <div className="calculator-logo-container">
        <img 
          className='calculator-logo'
          src={calculatorLogo}
          alt='Logo de Calcualdora'
        />
      </div>
      <div className='calculator-container'>
        <Button>1</Button>
        <Button>+</Button>
      </div>
    </div>
  );
}

export default App;
